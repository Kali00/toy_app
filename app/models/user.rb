class User < ApplicationRecord
	has_many :microposts
	validates :name, length: { maximum: 20 }, presence: true
	validates :email, :email_format => {:message => 'your email adress is incorrect! You should change it. :)'}, presence: true
end
